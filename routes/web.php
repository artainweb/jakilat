<?php

use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\primaryController;
use App\Http\Controllers\tertiaryController;
use App\Http\Controllers\secondaryController;
use App\Http\Controllers\BackEnd\LoginController;
use App\Http\Controllers\BackEnd\MemberController;
use App\Http\Controllers\BackEnd\RegisterController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// primaryController
Route::get('/',               [primaryController::class, 'index'])->name('home');
Route::get('/homeJakilat',    [primaryController::class, 'index'])->name('home');
Route::get('/tentangKami',    [primaryController::class, 'tentangKami'])->name('tentangKami');
Route::get('/gabungMember',   [primaryController::class, 'gabungMember'])->name('gabungMember');
Route::get('/ketentuan',      [primaryController::class, 'ketentuan'])->name('ketentuan');
Route::get('/gabungCustomer', [primaryController::class, 'gabungCustomer'])->name('gabungCustomer');
Route::get('/login',          [primaryController::class, 'login'])->name('login');
Route::get('/berandaUtama',   [primaryController::class, 'berandaUtama'])->name('berandaUtama');

Route::middleware(['auth', 'cekRole:Customer'])->group(function () {
    // secondaryController berisi tampilan cust
    Route::get('/beranda',               [secondaryController::class, 'beranda'])->name('beranda');
    Route::get('/deskripsi',             [secondaryController::class, 'deskripsi'])->name('deskripsi');
    Route::get('/editBiodata',           [secondaryController::class, 'editBiodata'])->name('editBiodata');
    Route::get('/editProyek',            [secondaryController::class, 'editProyek'])->name('editProyek');
    Route::get('/tambahProyek',          [secondaryController::class, 'tambahProyek'])->name('tambahProyek');
    Route::get('/buktiBayar',            [secondaryController::class, 'buktiBayar'])->name('buktiBayar');
    Route::get('/detailProyekPengajuan', [secondaryController::class, 'detailProyekPengajuan'])->name('detailProyekPengajuan');
    Route::get('/tambahProyekPengajuan', [secondaryController::class, 'tambahProyekPengajuan'])->name('tambahProyekPengajuan'); 
});

Route::middleware(['auth', 'cekRole:Member'])->group(function () {
    // tertiaryController berisi tampilan member
    Route::get('/berandaMember',    [tertiaryController::class, 'berandaMember'])->name('berandaMember');
    Route::get('/profileMember',    [tertiaryController::class, 'profileMember'])->name('profileMember');
    Route::get('/iklan',            [tertiaryController::class, 'iklan'])->name('iklan');
    Route::get('/proyekPengajuan',  [tertiaryController::class, 'proyekPengajuan'])->name('proyekPengajuan');

});

//---------------------------------------BACKEND---------------------------------------------------------
// REGISTER
Route::post('/saveMember',   [RegisterController::class, 'save_member'])->name('saveMember');
Route::post('/saveCustomer', [RegisterController::class, 'save_customer'])->name('saveCustomer');

//LOGIN
Route::post('/postLogin', [LoginController::class, 'postlogin'])->name('postLogin');

//MEMBER
Route::get('/terimaOrder/{id}/{terima_order}', [MemberController::class, 'terima_order'])->name('terimaOrder');
Route::get('/editBiodataMember/{id}',[MemberController::class, 'edit_biodata_member'])->name('editBiodataMember');
Route::post('/updateBiodataMember/{id}',[MemberController::class, 'update_biodata_member'])->name('updateBiodataMember');
Route::post('/tambahGambar', [MemberController::class, 'tambah_gambar'])->name('tambahGambarMember');

Route::post('/iklankanMember', [MemberController::class, 'iklan_member'])->name('IklankanMember');