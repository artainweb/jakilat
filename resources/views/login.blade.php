
@extends('layout/master')


@section('tittle', 'Login')


@section('content')

<br><br>
<div class="container">
<h1 class="uk-heading-line uk-text-center fw-bold"><span>Login Pengguna</span></h1>
</div><br>

<div class="container">
<div class="d-flex justify-content-center">
<div class="uk-card uk-card-default uk-card-large uk-card-body">
<ul class="uk-subnav uk-subnav-pill" uk-switcher="animation: uk-animation-fade">
    <li><a href="#">Customer</a></li>
    <li><a href="#">Member</a></li>
</ul>

<ul class="uk-switcher uk-margin">
   <!-- Login Pengguna  -->
    <li>
    <form action="{{route('postLogin')}}" method="POST">
        @csrf
        <div class="uk-margin">
            <div class="uk-inline">
                <span class="uk-form-icon" uk-icon="icon: user"></span>
                <input name="email" class="uk-input @error('email') is-invalid @enderror" placeholder="Email"  type="text">
                <span class ="text-danger">@error('email') {{$message}} @enderror</span>
            </div>
        </div>
        <div class="uk-margin">
            <div class="uk-inline">
                <span class="uk-form-icon uk-form-icon-flip" uk-icon="icon: lock"></span>
                <input name="password" class="uk-input @error('password') is-invalid @enderror" placeholder="Password" type="text">
                <span class ="text-danger">@error('password') {{$message}} @enderror</span>
                <input name="role" value="Customer" type="hidden" hidden>
            </div>
        </div>
        <button type="submit" class="uk-button uk-button-primary">Masuk</button>
    </form>
        <p>Belum punya akun ? <a href="gabungCustomer"> Daftar</a></p>
    </li>


    <!-- Login Member -->
    <li>
    <form action="{{route('postLogin')}}" method="POST">
        @csrf
    <div class="uk-margin">
            <div class="uk-inline">
                <span class="uk-form-icon" uk-icon="icon: user"></span>
                <input name="email"class="uk-input"  placeholder="Email" type="text">
            </div>
        </div>
        <div class="uk-margin">
            <div class="uk-inline">
                <span class="uk-form-icon uk-form-icon-flip" uk-icon="icon: lock"></span>
                <input name="password" class="uk-input" placeholder="Password" type="text">
                <input name="role" value="Member" type="hidden" hidden>
            </div>
        </div>
        <button type="submit" class="uk-button uk-button-primary">Masuk</button>
    </form>
        <p>Belum punya akun ? <a href="gabungMember"> Daftar</a></p>
    </li>
</ul>
</div>
</div>
</div>


@endsection