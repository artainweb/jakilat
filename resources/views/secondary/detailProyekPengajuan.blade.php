
@extends('layout/master')


@section('tittle', 'Detail Proyek Pengajuan')


@section('content')

<br>
<div class="container">
<h1 class="uk-heading-line uk-text-center fw-bold"><span>Pembangunan Taman</span></h1>

    <br>
    <p> Jika menemukan yang terbaik, anda dapat langsung menghubungi mereka melalui kontak yang tersedia di berkas.</p>
    <hr>

    <table class="uk-table">
    <thead>
        <tr>
            <th>Nama Penawar</th>
            <th>Harga Penawaran</th>
            <th>Berkas Penawaran</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>PT. AADDCC</td>
            <td><a href="#" class="warnaHijau">480.000.000</a></td>
            <td><a href="#" class="warnaBiru" uk-icon="cloud-download" uk-tooltip="tekan untuk unduh"></a></td>
        </tr>
        <!-- Contoh 2 -->
        <tr>
            <td>Johan</td>
            <td><a href="#" class="warnaHijau">486.000.000</a></td>
            <td><a href="#" class="warnaBiru" uk-icon="cloud-download" uk-tooltip="tekan untuk unduh"></a></td>
        </tr>
        <!-- Akhir dari contoh -->
    </tbody>
</table>
</div>


@endsection