
@extends('layout/master')


@section('tittle', 'Beranda')


@section('content')

<br><br>
<div class="container">

<article class="uk-comment uk-comment-primary">
    <header class="uk-comment-header">
        <div class="uk-grid-medium uk-flex-middle" uk-grid>
            <div class="uk-width-auto">
                <img class="uk-comment-avatar" src="{{ asset('asset/img/avatar.png') }}" width="80" height="80" alt="">
            </div>
            <div class="uk-width-expand">
                <h4 class="uk-comment-title uk-margin-remove"><a class="uk-link-reset" href="#">Nama Customer</a></h4>
                <ul class="uk-comment-meta uk-subnav uk-subnav-divider uk-margin-remove-top">
                    <li uk-tooltip="Hubungi Kontak Kami Jika Status Anda Dibekukan"><a href="#">Status : Aktif</a></li>
                    <li><a href="editBiodata">Edit Biodata</a></li>
                </ul>
            </div>
        </div>
    </header>
</article>

<div class="container">
    <hr>
    <div class="uk-card uk-card-default uk-card-body ">
        <ul class="uk-nav-default uk-nav-parent-icon" uk-nav>
            <li class="uk-active"><a href="#">Tindakan</a></li>
            <li class="uk-parent">
                <a href="#">Penjelasan</a>
                <ul class="uk-nav-sub">
                    <li><a href="#" uk-toggle="target: #modal-jasa" >Alur Proyek Pilih Jasa/Tukang</a></li>
                    <li><a href="#" uk-toggle="target: #modal-pengajuan">Alur Proyek Pengajuan Harga</a></li>
                    <li><a href="#" uk-toggle="target: #modal-status">Penjelasan Status Anda dan Proyek</a></li>
                </ul>
            </li>
            <li class="uk-parent">
                <a href="#">Tambah Proyek</a>
                <ul class="uk-nav-sub">
                    <li><a href="tambahProyek">Proyek Pilih Jasa/Tukang</a></li>
                    <li><a href="tambahProyekPengajuan">Proyek Pengajuan Harga </a></li>
                </ul>
            </li>
            <li class="uk-nav-divider"></li>
            <li class="uk-nav-header">Lainnya</li>
            
            <li><a href="deskripsi"><span class="uk-margin-small-right warnaBiru" uk-icon="icon: search"></span> Lihat Jasa Tersedia</a></li>
            <li><a href="buktiBayar"><span class="uk-margin-small-right warnaBiru" uk-icon="icon: cloud-upload"></span> Upload Bukti Pembayaran</a></li>
            <li><a href="https://wa.me/6289619715773"><span class="uk-margin-small-right warnaBiru" uk-icon="icon: receiver"></span>Hubungi CS</a></li>
        </ul>
    </div>
<br>
    <h3 class="text-center">Proyek Anda</h3>
    <hr>
    <p> Proyek Pilih Jasa/Tukang</p>

    <table class="uk-table">
    <thead>
        <tr>
            <th>Nama Proyek</th>
            <th>Status</th>
            <th>Tindakan</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Pembangunan Jalan</td>
            <td>Aktif</td>
            <td uk-tooltip="Tindakan Lihat tidak akan bisa dilakukan jika proyek berstatus selesai." ><a href="editProyek"> Lihat </a></td>
        </tr>
        <!-- Silahkan dihapus ini hanya contoh jika proyek selesai : tidakan edit tidak tersedia -->
        <tr>
            <td>Pembangunan Rumah Susun</td>
            <td>Selesai</td>
            <td uk-tooltip="Tindakan Lihat tidak akan bisa dilakukan jika proyek berstatus on-progress." ><a href=""> </a></td>
        </tr>
        <!-- Akhir dari contoh -->
    </tbody>
</table>
    <br>
    <p> Proyek Pengajuan Harga</p>

    <table class="uk-table">
    <thead>
        <tr>
            <th>Nama Proyek</th>
            <th>Penawaran</th>
            <th>Hapus</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Pembangunan Taman</td>
            <td><a href="detailProyekPengajuan" class="warnaHijau" uk-icon="folder" uk-tooltip="Lihat Detail"></a></td>
            <td><a href="#" uk-icon="trash" class="warnaMerah" uk-tooltip="Hapus"></a></td>
        </tr>
        <!-- Detail Harus Selalu Tersedia -->
        <tr>
            <td>Pembangunan Rumah 3 Lantai</td>
            <td><a href="detailProyekPengajuan" class="warnaHijau" uk-icon="folder" uk-tooltip="Lihat Detail"></a></td>
            <td><a href="#" uk-icon="trash" class="warnaMerah" uk-tooltip="Hapus"></a></td>
        </tr>
        <!-- Akhir dari contoh -->
    </tbody>
</table>
</div>

</div><br>


<!-- Modal Alur Proyek Pilih Jasa/Tukang -->
<div id="modal-jasa" uk-modal>
    <div class="uk-modal-dialog uk-modal-body">
        
        <p>Berikut adalah alur memesan jasa kontruksi/tukang kami : </p>
        <ul>
        <li>Tambah Proyek yang akan dikerjakan.</li>
        <li>Lihat Jasa Tersedia untuk menambahkan jasa kotruksi/tukang pilihan anda kedalam proyek tersebut.</li>
        <li>Jika dirasa sudah lengkap silahkan tekan submit proyek.</li>
        <li>Tim Jakilat akan me-review dan menghubungi tukang pilihan anda proses paling lambat 3x24 jam.</li>
        <li>Jika jasa/tukang yang anda pilih berhalangan maka anda dapat memilih kembali pada pilihan lihat jasa tersedia pada menu lainnya..</li>
        <li>Melakukan pembayaran biaya admin dan pengurusan berkas kepada PT. Bara Karya Sarana sebesar Rp. 10.000-, kemudian upload pada menu Upload Bukti Pembayaran.</li>
        <li>Pembayaran akan di proses oleh TIM 1x24 jam pada hari kerja.</li>
        <li>Jika ACC maka proyek anda siap untuk dikerjakan.</li>
        <li>Mohon validasi Proyek Telah Rampung jika proyek telah selesai dikerjakan, jika terdapat keluhan kami akan menghubungi, jika 3x24 jam tidak terdapat tanggapan maka kami akan mengubah status tanpa perberitahuan lebih lanjut.</li>
        </ul>
        
        <p class="uk-text-right">
            <button class="uk-button uk-button-default uk-modal-close" type="button">Tutup</button>
        </p>
        <br><br><br>
    </div>
</div>

<!-- Modal Alur Proyek pengajuan harga -->
<div id="modal-pengajuan" uk-modal>
    <div class="uk-modal-dialog uk-modal-body">
        
        <p>Berikut adalah alur proyek pengajuan harga : </p>
        <ul>
        <li>Tambah Proyek yang akan dikerjakan.</li>
        <li>Masukkan detail pada form yang telah disediakan.</li>
        <li>Pada upload berkas, silahkan masukkan dokumen pendukung, foto, ataupun lain sebagainya dalam satu PDF.</li>
        <li>Form tidak dapat diedit, jika anda merasa salah input, maka anda dapat hapus dan input ulang form.</li>
        <li>Jika dirasa sudah sesuai maka lakukan pembayaran biaya admin dan pengurusan berkas kepada PT. Bara Karya Sarana sebesar Rp. 10.000-, kemudian upload pada menu Upload Bukti Pembayaran</li>
        <li>Tim Jakilat akan me-review selama 1x24 jam pada hari kerja dan mulai mem-posting pengajuan anda selama 1 bulan.</li>
        <li>Anda dapat melihat siapa saja yang melakukan pengajuan penawaran pada menu PENAWARAN pada proyek pengajuan harga.</li>
        <li>Anda dapat menghubungi secara langsung pemilik penawaran yang melakukan pengajuan penawaran.</li>
        <li>Jika dalam 1 bulan tidak terdapat pengajuan penawaran atau tidak meperoleh hasil yang memuaskan, silahkan hubungi CS untuk meminta perpanjangan waktu.</li>
        </ul>
        
        <p class="uk-text-right">
            <button class="uk-button uk-button-default uk-modal-close" type="button">Tutup</button>
        </p>
        <br><br><br>
    </div>
</div>

<!-- Modal Status -->
<div id="modal-status" uk-modal>
    <div class="uk-modal-dialog uk-modal-body">
        
        <p>Penjelasan terhadap Status Anda : </p>
        <ul>
        <li>AKTIF : Anda berhasil mendaftar dan telah disetujui oleh kami untuk dapat membuat proyek.</li>
        <li>Dibekukan : Terdapat masalah terhadap profile anda, anda harus menghubingi Customer Support.</li>

        <p> Penjelasan terhadap Status Proyek Anda : </p>
        <li>Draf : Proyek anda sedang di review oleh TIM kami.</li>
        <li>Menunggu Pembayaran : Silahkan untuk melakukan pembayaran dan upload bukti pembayaran.</li>
        <li>Aktif : Proyek anda sudah/sedang dikerjakan.</li>
        <li>Selesai : Proyek anda telah rampung.</li></ul>
        
        <p class="uk-text-right">
            <button class="uk-button uk-button-default uk-modal-close" type="button">Tutup</button>
        </p>
        <br><br><br>
    </div>
</div>

@endsection