@extends('layout/master')


@section('tittle', 'Tambah Proyek')


@section('content')

<br><br>

<div class="container">

<!-- Form -->
<div class="text-center">
    <h1 class="fw-bold">Tambahkan Proyek Pilih Jasa/Tukang</h1>
    <p>Isi data dibawah dengan benar</p>
</div>

<form class="row g-3">
<div class="col-md-12">
    <label for="inputNama" class="form-label">Nama Proyek</label>
    <input type="text" class="form-control" placeholder="Pembangunan Jalan" id="inputNama" required>
  </div>
  <div class="col-12">
    <label for="inputAddress" class="form-label">Alamat Proyek</label>
    <input type="text" class="form-control" id="inputAddress" placeholder="Jl. Merdeka Barat" required>
  </div>
  <div class="col-12">
  <div>
        <h4>Jasa Yang Anda Butuhkan</h4>
        <ul class="uk-list uk-list-disc">
            <li> 
                <a href="profileTukang"> Johan | Tukang Semen </a>  
                <a href="" uk-icon="icon: trash" uk-tooltip="Gunakan untuk membatalkan pilihan"></a>           
            </li>
            <!-- Sebagai contoh jika ada 3 tukang silahkan hapus sisanya -->
            <li> 
                <a href="profileTukang"> Alex | Tukang Listrik </a>  
                <a href="" uk-icon="icon: trash" uk-tooltip="Gunakan untuk membatalkan pilihan"></a>           
            </li>
            <li> 
                <a href="profileTukang"> CV. Indraa | Jasa Aspal Jalan </a>  
                <a href="" uk-icon="icon: trash" uk-tooltip="Gunakan untuk membatalkan pilihan"></a>           
            </li>
            <!-- Hapus sampai sini -->
        </ul>
    </div>
  </div>
  <a type="button" href="deskripsi" class="btn btn-outline-warning">Cari Jasa/Tukang</a>
  <div class="col-12">
  <p>Status Proyek Anda :</p>
  <input uk-tooltip="Status Proyek Anda Akan Draf hingga semua sudah terlengkapi." class="uk-input uk-form-controls" type="text" value="Draf" disabled>
  <p>Setelah data dipastikan benar, kami akan menyiapkan jasa/tukang pilihan anda dan segera menghubungi anda.</p>
  <div class="col-12">
    <button type="submit" class="btn btn-primary">Submit</button>
  </div>

  </div></div>
</form>
</div>

@endsection