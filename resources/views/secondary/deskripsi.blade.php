
@extends('layout/master')


@section('tittle', 'Deskripsi')


@section('content')

<br><br>

<div class="container">
<div class="card shadow">
    <div class="card-body">
        <p class="text-center">Filter Pencarian Anda</p>
<form class="uk-grid-small" uk-grid>
    <div class="uk-width-1-2@s">
            <select class="uk-select">
                <option>Pilih Kategori</option>
                <option>Option 02</option>
            </select>
    </div>
    <div class="uk-width-1-2@s">
            <select class="uk-select">
                <option>Pilih Wilayah</option>
                <option>Option 02</option>
            </select>
    </div>
</form><p></p>
<div class="text-center">
<button type="button" class="btn btn-info">Submit</button>
</div>
</div>
</div>
</div>

<br><br>
<div class="container">
<h1 class="uk-heading-line uk-text-center fw-bold"><span>Member Teratas</span></h1>

<div class="row row-cols-1 row-cols-md-3 g-4">
  <div class="col">
    <div class="card shadow h-100">
    <div class="uk-card-header">
        <div class="uk-grid-small uk-flex-middle" uk-grid>
            <div class="uk-width-auto">
                <img class="uk-border-circle" width="40" height="40" src="{{ asset('asset/img/avatar.png') }}">
            </div>
            <div class="uk-width-expand">
                <h3 class="uk-card-title uk-margin-remove-bottom">Nama Tukang</h3>
                <p class="uk-text-meta uk-margin-remove-top">Tukang Listrik | Wilayah</p>
            </div>
        </div>
    </div>
    <div class="uk-card-body">
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.</p>
    </div>
    <div class="uk-card-footer">
        <a href="profileMember" class="uk-button uk-button-text">Pesan Jasa</a>
    </div>
    </div>
  </div>

<!-- Hapus dari sini -->
  <div class="col">
    <div class="card shadow h-100">
    <div class="uk-card-header">
        <div class="uk-grid-small uk-flex-middle" uk-grid>
            <div class="uk-width-auto">
                <img class="uk-border-circle" width="40" height="40" src="{{ asset('asset/img/avatar.png') }}">
            </div>
            <div class="uk-width-expand">
                <h3 class="uk-card-title uk-margin-remove-bottom">Nama Tukang</h3>
                <p class="uk-text-meta uk-margin-remove-top">Kategori | Kuburaya</p>
            </div>
        </div>
    </div>
    <div class="uk-card-body">
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.</p>
    </div>
    <div class="uk-card-footer">
        <a href="#" class="uk-button uk-button-text">Pesan Jasa</a>
    </div>
    </div>
  </div>

  <div class="col">
    <div class="card shadow h-100">
    <div class="uk-card-header">
        <div class="uk-grid-small uk-flex-middle" uk-grid>
            <div class="uk-width-auto">
                <img class="uk-border-circle" width="40" height="40" src="{{ asset('asset/img/avatar.png') }}">
            </div>
            <div class="uk-width-expand">
                <h3 class="uk-card-title uk-margin-remove-bottom">Nama Tukang</h3>
                <p class="uk-text-meta uk-margin-remove-top">Kategori | Wilayah</p>
            </div>
        </div>
    </div>
    <div class="uk-card-body">
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.</p>
    </div>
    <div class="uk-card-footer">
        <a href="#" class="uk-button uk-button-text">Pesan Jasa</a>
    </div>
    </div>
  </div>

  <div class="col">
    <div class="card shadow h-100">
    <div class="uk-card-header">
        <div class="uk-grid-small uk-flex-middle" uk-grid>
            <div class="uk-width-auto">
                <img class="uk-border-circle" width="40" height="40" src="{{ asset('asset/img/avatar.png') }}">
            </div>
            <div class="uk-width-expand">
                <h3 class="uk-card-title uk-margin-remove-bottom">Nama Tukang</h3>
                <p class="uk-text-meta uk-margin-remove-top">Kategori | Wilayah</p>
            </div>
        </div>
    </div>
    <div class="uk-card-body">
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.</p>
    </div>
    <div class="uk-card-footer">
        <a href="#" class="uk-button uk-button-text">Pesan Jasa</a>
    </div>
    </div>
  </div>
  <!-- sampai sini -->

</div>

</div><br>

<div class="container">
<h1 class="uk-heading-line uk-text-center fw-bold"><span>Semua Member</span></h1>

<div class="row row-cols-1 row-cols-md-3 g-4">
  <div class="col">
    <div class="card shadow h-100">
    <div class="uk-card-header">
        <div class="uk-grid-small uk-flex-middle" uk-grid>
            <div class="uk-width-auto">
                <img class="uk-border-circle" width="40" height="40" src="{{ asset('asset/img/avatar.png') }}">
            </div>
            <div class="uk-width-expand">
                <h3 class="uk-card-title uk-margin-remove-bottom">Nama Tukang</h3>
                <p class="uk-text-meta uk-margin-remove-top">Kategori | Wilayah</p>
            </div>
        </div>
    </div>
    <div class="uk-card-body">
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.</p>
    </div>
    <div class="uk-card-footer">
        <a href="#" class="uk-button uk-button-text">Pesan Jasa</a>
    </div>
    </div>
  </div>

<!-- Hapus dari sini -->
  <div class="col">
    <div class="card shadow h-100">
    <div class="uk-card-header">
        <div class="uk-grid-small uk-flex-middle" uk-grid>
            <div class="uk-width-auto">
                <img class="uk-border-circle" width="40" height="40" src="{{ asset('asset/img/avatar.png') }}">
            </div>
            <div class="uk-width-expand">
                <h3 class="uk-card-title uk-margin-remove-bottom">Nama Tukang</h3>
                <p class="uk-text-meta uk-margin-remove-top">Kategori | Pontianak</p>
            </div>
        </div>
    </div>
    <div class="uk-card-body">
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.</p>
    </div>
    <div class="uk-card-footer">
        <a href="#" class="uk-button uk-button-text">Pesan Jasa</a>
    </div>
    </div>
  </div>

  <div class="col">
    <div class="card shadow h-100">
    <div class="uk-card-header">
        <div class="uk-grid-small uk-flex-middle" uk-grid>
            <div class="uk-width-auto">
                <img class="uk-border-circle" width="40" height="40" src="{{ asset('asset/img/avatar.png') }}">
            </div>
            <div class="uk-width-expand">
                <h3 class="uk-card-title uk-margin-remove-bottom">Nama Tukang</h3>
                <p class="uk-text-meta uk-margin-remove-top">Tukang Las | Wilayah</p>
            </div>
        </div>
    </div>
    <div class="uk-card-body">
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.</p>
    </div>
    <div class="uk-card-footer">
        <a href="#" class="uk-button uk-button-text">Pesan Jasa</a>
    </div>
    </div>
  </div>

  <div class="col">
    <div class="card shadow h-100">
    <div class="uk-card-header">
        <div class="uk-grid-small uk-flex-middle" uk-grid>
            <div class="uk-width-auto">
                <img class="uk-border-circle" width="40" height="40" src="{{ asset('asset/img/avatar.png') }}">
            </div>
            <div class="uk-width-expand">
                <h3 class="uk-card-title uk-margin-remove-bottom">Nama Tukang</h3>
                <p class="uk-text-meta uk-margin-remove-top">Kategori | Wilayah</p>
            </div>
        </div>
    </div>
    <div class="uk-card-body">
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.</p>
    </div>
    <div class="uk-card-footer">
        <a href="#" class="uk-button uk-button-text">Pesan Jasa</a>
    </div>
    </div>
  </div>
  <!-- sampai sini -->

</div>

</div><br>



@endsection