@extends('layout/master')


@section('tittle', 'Tambah Proyek Pengajuan')


@section('content')

<br><br>

<div class="container">

<!-- Form -->
<div class="text-center">
    <h1 class="fw-bold">Tambahkan Proyek Pengajuan</h1>
    <p>Isi data dibawah dengan benar</p>
</div>

<form class="row g-3">
  <div class="col-md-6">
    <label for="inputNama" class="form-label">Nama Proyek</label>
    <input type="text" class="form-control" placeholder="Cukup Tiga Kata" id="inputNama" required>
  </div>
  <div class="col-md-6">
    <label for="inputTanggal" class="form-label">Batas Penerimaan Penawaran</label>
    <input type="date" class="form-control" id="inputTanggal" required>
  </div>
  <div class="col-12">
    <label for="inputAddress" class="form-label">Lokasi Proyek</label>
    <input type="text" class="form-control" id="inputAddress" placeholder="Masukkan Alamat Proyek Dilaksanakan" required>
  </div>
  <div class="col-md-6">
    <label for="inputHarga" class="form-label">Input Harga Pembukaan</label>
    <input type="number" placeholder="Contoh : 500.000.000" class="form-control" id="inputHarga" required>
  </div>
  <div class="col-md-6" uk-tooltip="Pastikan Gambar Terlihat Jelas">
    <label for="inputBukti" class="form-label">Upload Berkas (jika ada)</label>
    <input type="file" class="form-control" id="inputBukti">
  </div>
  <div class="form-floating">
    <textarea class="form-control" placeholder="Leave a comment here" id="floatingTextarea2" style="height: 100px"></textarea>
    <label for="floatingTextarea2">Deskripsikan Proyek Anda</label>
  </div>
  <div class="col-12">
  <div class="form-check">
      <input class="form-check-input is-invalid" type="checkbox" value="" id="invalidCheck3" aria-describedby="invalidCheck3Feedback" required>
      <label class="form-check-label" for="invalidCheck3">
        Dengan sumbit anda telah menyetujui <a href="ketentuan">syarat dan ketentuan</a>  yang tersedia.
      </label>
      <div id="invalidCheck3Feedback" class="invalid-feedback">
        Centang kotak untuk dapat lanjut .... 
      </div>
    </div>
  </div>
  <p>Setelah data terkirim silahkan kalukan pembayaran Rp. 10.000-, dan upload bukti.</p>
  <div class="col-6">
    <button type="submit" class="btn btn-primary">Submit</button>
  </div>
  <div class="col-6">
    <button onclick="goBack()" class="btn btn-warning">Batal </button>
  </div>






</form>
</div>

@endsection