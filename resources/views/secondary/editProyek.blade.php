
@extends('layout/master')


@section('tittle', 'Edit Proyek')



@section('content')

<br><br>

<div class="container">

<!-- Form -->
<div class="text-center">
    <h1 class="fw-bold">Lihat Detail Status Proyek</h1>
    <p>Pastikan Data Sudah Benar</p>
</div>

<form class="row g-3">
  <div class="col-md-12">
    <label for="inputNama" class="form-label">Nama Proyek</label>
    <input type="text" class="form-control" placeholder="Pembangunan Jalan" id="inputNama" required>
  </div>
  <div class="col-12">
    <label for="inputAddress" class="form-label">Alamat Proyek</label>
    <input type="text" class="form-control" id="inputAddress" placeholder="Jl. Merdeka Barat" required>
  </div>
  <div class="col-12">
  <div>
        <h4>Jasa Yang Anda Butuhkan</h4>
        <ul class="uk-list uk-list-disc">
            <li> 
                <a href="profileTukang"> Johan | Tukang Semen </a>            
            </li>
            <!-- Sebagai contoh jika ada 3 tukang silahkan hapus sisanya -->
            <li> 
                <a href="profileTukang"> Alex | Tukang Listrik </a>            
            </li>
            <li> 
                <a href="profileTukang"> CV. Indraa | Jasa Aspal Jalan </a>           
            </li>
            <!-- Hapus sampai sini -->
        </ul>
    </div>
  </div>
  <div class="col-12">
  <p>Status Proyek Anda :</p>
  <input class="uk-input uk-form-controls" type="text" value="Aktif" disabled>
  </div>
  <p>Mohon untuk validasi proyek telah rampung jika proyek sudah selesai dibangun agar pekerja dapat kembali menerima orderan.</p>
  <div class="col-12">
    <button type="submit" class="btn btn-primary">Proyek Telah Rampung</button>
  </div>

</form>
</div>

@endsection