
@extends('layout/masterUtama')


@section('tittle', 'Beranda Utama')


@section('content')

<br><br>

<div class="container">
<div class="card shadow">
    <div class="card-body">
        <p class="text-center">Filter Pencarian Anda</p>
<form class="uk-grid-small" uk-grid>
    <div class="uk-width-1-2@s">
            <select class="uk-select">
                <option>Pilih Kategori</option>
                <option>Option 02</option>
            </select>
    </div>
    <div class="uk-width-1-2@s">
            <select class="uk-select">
                <option>Pilih Wilayah</option>
                <option>Option 02</option>
            </select>
    </div>
</form><p></p>
<div class="text-center">
<button type="button" class="btn btn-info">Submit</button>
</div>
</div>
</div>
</div>

<br>

<div class="container">
<div class="d-grid gap-2">
<a href="login" type="button" class="btn btn-outline-primary">Lihat Lebih Banyak </a>
</div>
</div>

<br>

<!-- Untuk Proyek Pengajuan -->
<div class="container">
<h1 class="uk-heading-line uk-text-center fw-bold"><span>Proyek Tersedia</span></h1>
<p class="text-center" >Member Dapat Mengajukan Penawaran Pada Proyek Ini</p>
<div uk-slider>
<div class="uk-position-relative uk-visible-toggle uk-dark" tabindex="-1">

    <ul class="uk-slider-items uk-child-width-1-1 uk-child-width-1-3@m uk-grid">
        <li>
            <div class="uk-panel">
            <div class="col">
                <div class="card shadow">
                <div class="container"><br>
                <ul class="uk-nav-default uk-nav-parent-icon" uk-nav>
                    <li class="uk-active"><a href="#">Nama Penyedia Proyek</a></li>
                    <li class="uk-parent">
                        <a href="#">Deskripsi Proyek</a>
                        <ul class="uk-nav-sub">
                            <li><a href="#">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Corrupti culpa, rerum ut sunt eos quis illo voluptatum nisi? Magnam eius ullam dolore inventore est totam reprehenderit. Itaque ea adipisci exercitationem.</a></li>
                        </ul>
                    </li>
                    <li class="uk-parent">
                        <a href="#">File Tambahan</a>
                        <ul class="uk-nav-sub">
                            <li><a href="login">Tekan untuk unduh</a></li>
                        </ul>
                    </li>
                    <li class="uk-nav-header">Pembangunan Taman Bermain</li>
                    <li><a href="#"><span class="uk-margin-small-right warnaBiru" uk-icon="icon: tag"></span> Harga Tertinggi : 100.000.000</a></li>
                    <li><a href="#"><span class="uk-margin-small-right warnaBiru" uk-icon="icon: calendar"></span> mm/dd/yyy</a></li>
                    <li><a href="#"><span class="uk-margin-small-right warnaBiru" uk-icon="icon: location"></span> Jl. Bukit Tinggi</a></li>
                    <li class="uk-nav-divider"></li>
                    <li><a href="login"><span class="uk-margin-small-right warnaBiru" uk-icon="icon: folder"></span> Ajukan Penawaran</a></li>
                </ul><br>
                </div>
                </div>
            </div>
            </div>
        </li>
        <!-- Hapus dari sini -->
        <li>
            <div class="uk-panel">
            <div class="col">
                <div class="card shadow">
                <div class="container"><br>
                <ul class="uk-nav-default uk-nav-parent-icon" uk-nav>
                    <li class="uk-active"><a href="#">Nama Penyedia Proyek</a></li>
                    <li class="uk-parent">
                        <a href="#">Deskripsi Proyek</a>
                        <ul class="uk-nav-sub">
                            <li><a href="#">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Corrupti culpa, rerum ut sunt eos quis illo voluptatum nisi? Magnam eius ullam dolore inventore est totam reprehenderit. Itaque ea adipisci exercitationem.</a></li>
                        </ul>
                    </li>
                    <li class="uk-parent">
                        <a href="#">File Tambahan</a>
                        <ul class="uk-nav-sub">
                            <li><a href="login">Tekan untuk unduh</a></li>
                        </ul>
                    </li>
                    <li class="uk-nav-header">Pembangunan Rumah 3lt</li>
                    <li><a href="#"><span class="uk-margin-small-right warnaBiru" uk-icon="icon: tag"></span> Harga Tertinggi : 470.000.000</a></li>
                    <li><a href="#"><span class="uk-margin-small-right warnaBiru" uk-icon="icon: calendar"></span> mm/dd/yyy</a></li>
                    <li><a href="#"><span class="uk-margin-small-right warnaBiru" uk-icon="icon: location"></span> Jl. Bukit Tinggi</a></li>
                    <li class="uk-nav-divider"></li>
                    <li><a href="login"><span class="uk-margin-small-right warnaBiru" uk-icon="icon: folder"></span> Ajukan Penawaran</a></li>
                </ul><br>
                </div>
                </div>
            </div>
            </div>
        </li>
        <li>
            <div class="uk-panel">
            <div class="col">
                <div class="card shadow">
                <div class="container"><br>
                <ul class="uk-nav-default uk-nav-parent-icon" uk-nav>
                    <li class="uk-active"><a href="#">Nama Penyedia Proyek</a></li>
                    <li class="uk-parent">
                        <a href="#">Deskripsi Proyek</a>
                        <ul class="uk-nav-sub">
                            <li><a href="#">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Corrupti culpa, rerum ut sunt eos quis illo voluptatum nisi? Magnam eius ullam dolore inventore est totam reprehenderit. Itaque ea adipisci exercitationem.</a></li>
                        </ul>
                    </li>
                    <li class="uk-parent">
                        <a href="#">File Tambahan</a>
                        <ul class="uk-nav-sub">
                            <li><a href="login">Tekan untuk unduh</a></li>
                        </ul>
                    </li>
                    <li class="uk-nav-header">Pembangunan Gudang</li>
                    <li><a href="#"><span class="uk-margin-small-right warnaBiru" uk-icon="icon: tag"></span> Harga Tertinggi : 500.000.000</a></li>
                    <li><a href="#"><span class="uk-margin-small-right warnaBiru" uk-icon="icon: calendar"></span> mm/dd/yyy</a></li>
                    <li><a href="#"><span class="uk-margin-small-right warnaBiru" uk-icon="icon: location"></span> Jl. Bukit Tinggi</a></li>
                    <li class="uk-nav-divider"></li>
                    <li><a href="login"><span class="uk-margin-small-right warnaBiru" uk-icon="icon: folder"></span> Ajukan Penawaran</a></li>
                </ul><br>
                </div>
                </div>
            </div>
            </div>
        </li>
        <li>
            <div class="uk-panel">
            <div class="col">
                <div class="card shadow">
                <div class="container"><br>
                <ul class="uk-nav-default uk-nav-parent-icon" uk-nav>
                    <li class="uk-active"><a href="#">Nama Penyedia Proyek</a></li>
                    <li class="uk-parent">
                        <a href="#">Deskripsi Proyek</a>
                        <ul class="uk-nav-sub">
                            <li><a href="#">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Corrupti culpa, rerum ut sunt eos quis illo voluptatum nisi? Magnam eius ullam dolore inventore est totam reprehenderit. Itaque ea adipisci exercitationem.</a></li>
                        </ul>
                    </li>
                    <li class="uk-parent">
                        <a href="#">File Tambahan</a>
                        <ul class="uk-nav-sub">
                            <li><a href="login">Tekan untuk unduh</a></li>
                        </ul>
                    </li>
                    <li class="uk-nav-header">Pembangunan Taman Bermain</li>
                    <li><a href="#"><span class="uk-margin-small-right warnaBiru" uk-icon="icon: tag"></span> Harga Tertinggi : 50.000.000.000</a></li>
                    <li><a href="#"><span class="uk-margin-small-right warnaBiru" uk-icon="icon: calendar"></span> mm/dd/yyy</a></li>
                    <li><a href="#"><span class="uk-margin-small-right warnaBiru" uk-icon="icon: location"></span> Jl. Bukit Tinggi</a></li>
                    <li class="uk-nav-divider"></li>
                    <li><a href="login"><span class="uk-margin-small-right warnaBiru" uk-icon="icon: folder"></span> Ajukan Penawaran</a></li>
                </ul><br>
                </div>
                </div>
            </div>
            </div>
        </li>
        <!-- Sampai sini -->
    </ul>

    <a class="uk-position-center-left uk-position-small warnaBiru" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
    <a class="uk-position-center-right uk-position-small warnaBiru" href="#" uk-slidenav-next uk-slider-item="next"></a>

</div>

<ul class="uk-slider-nav uk-dotnav uk-flex-center uk-margin"></ul>
</div>
</div>


<!-- Untuk Jasa/Tukang -->
<br><br>
<div class="container">
<h1 class="uk-heading-line uk-text-center fw-bold"><span>Jasa Tersedia</span></h1>
<p class="text-center">Customer Dapat Memilih Member Sesuai Kebutuhan Untuk Bekerja</p>

<div class="row row-cols-1 row-cols-md-3 g-4">
  <div class="col">
    <div class="card shadow h-100">
    <div class="uk-card-header">
        <div class="uk-grid-small uk-flex-middle" uk-grid>
            <div class="uk-width-auto">
                <img class="uk-border-circle" width="40" height="40" src="{{ asset('asset/img/avatar.png') }}">
            </div>
            <div class="uk-width-expand">
                <h3 class="uk-card-title uk-margin-remove-bottom">Nama Tukang</h3>
                <p class="uk-text-meta uk-margin-remove-top">Tukang Listrik | Wilayah</p>
            </div>
        </div>
    </div>
    <div class="uk-card-body">
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.</p>
    </div>
    <div class="uk-card-footer">
        <a href="login" class="uk-button uk-button-text">Pesan Jasa</a>
    </div>
    </div>
  </div>

<!-- Hapus dari sini -->
  <div class="col">
    <div class="card shadow h-100">
    <div class="uk-card-header">
        <div class="uk-grid-small uk-flex-middle" uk-grid>
            <div class="uk-width-auto">
                <img class="uk-border-circle" width="40" height="40" src="{{ asset('asset/img/avatar.png') }}">
            </div>
            <div class="uk-width-expand">
                <h3 class="uk-card-title uk-margin-remove-bottom">Nama Tukang</h3>
                <p class="uk-text-meta uk-margin-remove-top">Kategori | Kuburaya</p>
            </div>
        </div>
    </div>
    <div class="uk-card-body">
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.</p>
    </div>
    <div class="uk-card-footer">
        <a href="login" class="uk-button uk-button-text">Pesan Jasa</a>
    </div>
    </div>
  </div>

  <div class="col">
    <div class="card shadow h-100">
    <div class="uk-card-header">
        <div class="uk-grid-small uk-flex-middle" uk-grid>
            <div class="uk-width-auto">
                <img class="uk-border-circle" width="40" height="40" src="{{ asset('asset/img/avatar.png') }}">
            </div>
            <div class="uk-width-expand">
                <h3 class="uk-card-title uk-margin-remove-bottom">Nama Tukang</h3>
                <p class="uk-text-meta uk-margin-remove-top">Kategori | Wilayah</p>
            </div>
        </div>
    </div>
    <div class="uk-card-body">
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.</p>
    </div>
    <div class="uk-card-footer">
        <a href="login" class="uk-button uk-button-text">Pesan Jasa</a>
    </div>
    </div>
  </div>

  <div class="col">
    <div class="card shadow h-100">
    <div class="uk-card-header">
        <div class="uk-grid-small uk-flex-middle" uk-grid>
            <div class="uk-width-auto">
                <img class="uk-border-circle" width="40" height="40" src="{{ asset('asset/img/avatar.png') }}">
            </div>
            <div class="uk-width-expand">
                <h3 class="uk-card-title uk-margin-remove-bottom">Nama Tukang</h3>
                <p class="uk-text-meta uk-margin-remove-top">Kategori | Wilayah</p>
            </div>
        </div>
    </div>
    <div class="uk-card-body">
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.</p>
    </div>
    <div class="uk-card-footer">
        <a href="login" class="uk-button uk-button-text">Pesan Jasa</a>
    </div>
    </div>
  </div>
  <!-- sampai sini -->

</div>

</div><br>


</div>

</div><br>



@endsection