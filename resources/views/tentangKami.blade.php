
@extends('layout/masterUtama')


@section('tittle', 'Tentang Kami')



@section('content')

<br><br>
<div class="container">
<h1 class="uk-heading-line uk-text-center fw-bold"><span>Tentang Kami</span></h1>
</div><br>


<!-- Deskripsi -->
<div class="container">
    <h1 class="text-center"> Apa Itu Jakilat </h1>
    <br>
    <ul class="list-unstyled text-justify">
    <li>
        <ul>
        <li>Jakilat adalah platform yang memungkinkan untuk pekerja lepas memperluas jangkauan jaringan pemasaran, kerjasama atau lainnya.</li>
        <li>Kami memastikan anggota kami yang terdaftar adalah tenaga ahli yang profesional pada bidangnya.</li>
        <li>Kami mengutamakan keamanan dan kenyamanan untuk para customer agar mendapatkan kebutuhan yang sesuai.</li>
        <li>Kami juga memastikan transaksi yang dilakukan aman dan flexibel.</li>
        </ul>
    </li>
    </ul>

    <h1 class="text-center font-h1"> Kontak Kami </h1>
    <p class="text-center">Hubungi Kami Akan Segera Membantu</p>
    <br>

    <div class="uk-grid-column-small uk-grid-row-large uk-child-width-1-3@s uk-text-center" uk-grid>
        <div>
            <div class="uk-card uk-card-default uk-card-body">
            <p uk-icon="receiver" ></p> <p></p>
             089619715773
            </div>
        </div>
        <div>
            <div class="uk-card uk-card-default uk-card-body">
            <p uk-icon="google" ></p> <p></p>
             cs@jakilat.com
            </div>
        </div>
        <div>
            <div class="uk-card uk-card-default uk-card-body">
            <p uk-icon="location" ></p> <p></p>
            Jl. Sui Raya Dalam Komplek BDN No.2, Kubu Raya, Kalimantan Barat.
            </div>
        </div>
    </div>

    <br>

    <!-- Testimoni -->
    <h1 class="text-center font-h1"> TESTIMONI </h1>
    <p class="text-center">Dengarkan Apa Kata Mereka</p>
    <br>

    <!-- Testimoni 1 -->
    <article class="uk-comment">
        <header class="uk-comment-header">
            <div class="uk-grid-medium uk-flex-middle" uk-grid>
                <div class="uk-width-auto">
                    <img class="uk-comment-avatar" src="{{ asset('asset/img/avatar.png') }}" width="80" height="80" alt="">
                </div>
                <div class="uk-width-expand">
                    <h4 class="uk-comment-title uk-margin-remove"><a class="uk-link-reset" href="#">Nama</a></h4>
                    <ul class="uk-comment-meta uk-subnav uk-subnav-divider uk-margin-remove-top">
                        <li>Jabatan</a></li>
                    </ul>
                </div>
            </div>
        </header>
        <div class="uk-comment-body">
            <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>
        </div>
    </article><br>

    <!-- Testimoni 2 -->
    <article class="uk-comment">
        <header class="uk-comment-header">
            <div class="uk-grid-medium uk-flex-middle" uk-grid>
                <div class="uk-width-auto">
                    <img class="uk-comment-avatar" src="{{ asset('asset/img/avatar.png') }}" width="80" height="80" alt="">
                </div>
                <div class="uk-width-expand">
                    <h4 class="uk-comment-title uk-margin-remove"><a class="uk-link-reset" href="#">Bara Karya Sarana</a></h4>
                    <ul class="uk-comment-meta uk-subnav uk-subnav-divider uk-margin-remove-top">
                        <li>Direktur</a></li>
                    </ul>
                </div>
            </div>
        </header>
        <div class="uk-comment-body">
            <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>
        </div>
    </article><br>


    <!-- Testimoni 3 -->
    <article class="uk-comment">
        <header class="uk-comment-header">
            <div class="uk-grid-medium uk-flex-middle" uk-grid>
                <div class="uk-width-auto">
                    <img class="uk-comment-avatar" src="{{ asset('asset/img/avatar.png') }}" width="80" height="80" alt="">
                </div>
                <div class="uk-width-expand">
                    <h4 class="uk-comment-title uk-margin-remove"><a class="uk-link-reset" href="#">Bara Karya Sarana</a></h4>
                    <ul class="uk-comment-meta uk-subnav uk-subnav-divider uk-margin-remove-top">
                        <li>Staff Ahli PT. BKS</a></li>
                    </ul>
                </div>
            </div>
        </header>
        <div class="uk-comment-body">
            <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>
        </div>
    </article>




</div><br>

@endsection