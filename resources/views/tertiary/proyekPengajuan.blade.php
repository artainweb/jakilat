
@extends('layout/master')


@section('tittle', 'proyek Pengajuan')


@section('content')

<br><br>

<div class="container">
<div class="card shadow">
    <div class="card-body">
        <p class="text-center">Filter Pencarian Anda</p>
<form class="uk-grid-small" uk-grid>
    <div class="uk-width-1-2@s">
            <select class="uk-select">
                <option>Pilih Kategori</option>
                <option>Option 02</option>
            </select>
    </div>
    <div class="uk-width-1-2@s">
            <select class="uk-select">
                <option>Pilih Wilayah</option>
                <option>Option 02</option>
            </select>
    </div>
</form><p></p>
<div class="text-center">
<button type="button" class="btn btn-info">Submit</button>
</div>
</div>
</div>
</div>

<br><br>
<div class="container">
<h1 class="uk-heading-line uk-text-center fw-bold"><span>Proyek Pengajuan</span></h1>

<div class="row row-cols-1 row-cols-md-3 g-4">
  <div class="col">
    <div class="card shadow">
    <div class="container"><br>
    <ul class="uk-nav-default uk-nav-parent-icon" uk-nav>
        <li class="uk-active"><a href="#">Nama Penyedia Proyek</a></li>
        <li class="uk-parent">
            <a href="#">Deskripsi Proyek</a>
            <ul class="uk-nav-sub">
                <li><a href="#">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Corrupti culpa, rerum ut sunt eos quis illo voluptatum nisi? Magnam eius ullam dolore inventore est totam reprehenderit. Itaque ea adipisci exercitationem.</a></li>
            </ul>
        </li>
        <li class="uk-parent">
            <a href="#">File Tambahan</a>
            <ul class="uk-nav-sub">
                <li><a href="#">Tekan untuk unduh</a></li>
            </ul>
        </li>
        <li class="uk-nav-header">Pembangunan Taman Bermain</li>
        <li><a href="#"><span class="uk-margin-small-right warnaBiru" uk-icon="icon: tag"></span> Harga Tertinggi : 500.000.000</a></li>
        <li><a href="#"><span class="uk-margin-small-right warnaBiru" uk-icon="icon: calendar"></span> mm/dd/yyy</a></li>
        <li><a href="#"><span class="uk-margin-small-right warnaBiru" uk-icon="icon: location"></span> Jl. Bukit Tinggi</a></li>
        <li class="uk-nav-divider"></li>
        <li><a uk-toggle="target: #modal-ajukan"><span class="uk-margin-small-right warnaBiru" uk-icon="icon: folder"></span> Ajukan Penawaran</a></li>
    </ul><br>
    </div>
    </div>
  </div>

  <!-- Hapus dari sini -->
  <div class="col">
    <div class="card shadow">
    <div class="container"><br>
    <ul class="uk-nav-default uk-nav-parent-icon" uk-nav>
        <li class="uk-active"><a href="#">Nama Penyedia Proyek</a></li>
        <li class="uk-parent">
            <a href="#">Deskripsi Proyek</a>
            <ul class="uk-nav-sub">
                <li><a href="#">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Corrupti culpa, rerum ut sunt eos quis illo voluptatum nisi? Magnam eius ullam dolore inventore est totam reprehenderit. Itaque ea adipisci exercitationem.</a></li>
            </ul>
        </li>
        <li class="uk-parent">
            <a href="#">File Tambahan</a>
            <ul class="uk-nav-sub">
                <li><a href="#">Tekan untuk unduh</a></li>
            </ul>
        </li>
        <li class="uk-nav-header">Pembangunan Taman Bermain</li>
        <li><a href="#"><span class="uk-margin-small-right warnaBiru" uk-icon="icon: tag"></span> Harga Tertinggi : 500.000.000</a></li>
        <li><a href="#"><span class="uk-margin-small-right warnaBiru" uk-icon="icon: calendar"></span> mm/dd/yyy</a></li>
        <li><a href="#"><span class="uk-margin-small-right warnaBiru" uk-icon="icon: location"></span> Jl. Bukit Tinggi</a></li>
        <li class="uk-nav-divider"></li>
        <li><a uk-toggle="target: #modal-ajukan"><span class="uk-margin-small-right warnaBiru" uk-icon="icon: folder"></span> Ajukan Penawaran</a></li>
    </ul><br>
    </div>
    </div>
  </div>

  <div class="col">
    <div class="card shadow">
    <div class="container"><br>
    <ul class="uk-nav-default uk-nav-parent-icon" uk-nav>
        <li class="uk-active"><a href="#">Nama Penyedia Proyek</a></li>
        <li class="uk-parent">
            <a href="#">Deskripsi Proyek</a>
            <ul class="uk-nav-sub">
                <li><a href="#">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Corrupti culpa, rerum ut sunt eos quis illo voluptatum nisi? Magnam eius ullam dolore inventore est totam reprehenderit. Itaque ea adipisci exercitationem.</a></li>
            </ul>
        </li>
        <li class="uk-parent">
            <a href="#">File Tambahan</a>
            <ul class="uk-nav-sub">
                <li><a href="#">Tekan untuk unduh</a></li>
            </ul>
        </li>
        <li class="uk-nav-header">Pembangunan Taman Bermain</li>
        <li><a href="#"><span class="uk-margin-small-right warnaBiru" uk-icon="icon: tag"></span> Harga Tertinggi : 500.000.000</a></li>
        <li><a href="#"><span class="uk-margin-small-right warnaBiru" uk-icon="icon: calendar"></span> mm/dd/yyy</a></li>
        <li><a href="#"><span class="uk-margin-small-right warnaBiru" uk-icon="icon: location"></span> Jl. Bukit Tinggi</a></li>
        <li class="uk-nav-divider"></li>
        <li><a uk-toggle="target: #modal-ajukan"><span class="uk-margin-small-right warnaBiru" uk-icon="icon: folder"></span> Ajukan Penawaran</a></li>
    </ul><br>
    </div>
    </div>
  </div>

  <div class="col">
    <div class="card shadow">
    <div class="container"><br>
    <ul class="uk-nav-default uk-nav-parent-icon" uk-nav>
        <li class="uk-active"><a href="#">Nama Penyedia Proyek</a></li>
        <li class="uk-parent">
            <a href="#">Deskripsi Proyek</a>
            <ul class="uk-nav-sub">
                <li><a href="#">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Corrupti culpa, rerum ut sunt eos quis illo voluptatum nisi? Magnam eius ullam dolore inventore est totam reprehenderit. Itaque ea adipisci exercitationem.</a></li>
            </ul>
        </li>
        <li class="uk-parent">
            <a href="#">File Tambahan</a>
            <ul class="uk-nav-sub">
                <li><a href="#">Tekan untuk unduh</a></li>
            </ul>
        </li>
        <li class="uk-nav-header">Pembangunan Taman Bermain</li>
        <li><a href="#"><span class="uk-margin-small-right warnaBiru" uk-icon="icon: tag"></span> Harga Tertinggi : 500.000.000</a></li>
        <li><a href="#"><span class="uk-margin-small-right warnaBiru" uk-icon="icon: calendar"></span> mm/dd/yyy</a></li>
        <li><a href="#"><span class="uk-margin-small-right warnaBiru" uk-icon="icon: location"></span> Jl. Bukit Tinggi</a></li>
        <li class="uk-nav-divider"></li>
        <li><a uk-toggle="target: #modal-ajukan" ><span class="uk-margin-small-right warnaBiru" uk-icon="icon: folder"></span> Ajukan Penawaran</a></li>
    </ul><br>
    </div>
    </div>
  </div>

  <!-- Sampai sini -->
</div>

<!-- Modal Alur Ajukan Penawaran-->
<div id="modal-ajukan" uk-modal>
    <div class="uk-modal-dialog uk-modal-body">
        
        <p>Isi form dibawah ini dengan benar : </p>
        <hr>
        <form class="row g-3">
        <div class="col-md-12">
            <label for="inputNomorRek" class="form-label">Angka Penawaran</label>
            <input type="number" class="form-control" placeholder="Contoh : 500.000.000" id="inputNomorRek" required>
        </div>
        <div class="col-md-12" uk-tooltip="Kirim dalam satu file PDF">
            <label for="inputBerkas" class="form-label">Upload Berkas Penawaran</label>
            <input type="file" class="form-control" id="inputBerkas" required>
        </div>
        <p>Setelah input berkas anda akan masuk ke customer.</p>
        </form>
        
        <p class="uk-text-right">
            <button class="uk-button uk-button-primary " type="button">Submit</button>
            <button class="uk-button uk-button-default uk-modal-close" type="button">Tutup</button>
        </p>
        <br><br><br>
    </div>
</div>


</div><br>



@endsection