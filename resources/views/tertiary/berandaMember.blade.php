
@extends('layout/master')


@section('tittle', 'Beranda Member')


@section('content')

<br><br>
<div class="container">

<article class="uk-comment uk-comment-primary">
    <header class="uk-comment-header">
        <div class="uk-grid-medium uk-flex-middle" uk-grid>
            <div class="uk-width-auto">
                <img class="uk-comment-avatar" src="../member/foto_diri/{{$dataMember->foto_diri}}" width="80" height="80" alt="">
            </div>
            <div class="uk-width-expand">
                <h4 class="uk-comment-title uk-margin-remove"><a class="uk-link-reset" href="#">{{Auth::user()->name}}</a></h4>
                <ul class="uk-comment-meta uk-subnav uk-subnav-divider uk-margin-remove-top">
                    <li uk-tooltip="Hubungi Kontak Kami Jika Status Anda Dibekukan">
                        
                        <a href="#">Status : 
                            @if($dataMember->status == 1)
                            Aktif
                            @else
                            Tidak Aktif
                            @endif
                        </a></li>
                </ul>
            </div>
        </div>
    </header>
    <div class="form-check form-switch">

        <input type="checkbox" role="switch" id="flexSwitchCheckChecked"  class="form-check-input cek" name="terima_order" value="{{$dataMember->terima_order}}" 
        data-id="{{$dataMember->id}}" member-id = "{{$dataMember->id}}" @if($dataMember->terima_order==1) checked @endif>

        {{-- <a href="#" class = "btn btn-danger btn-sm cek" id="terima_order" name="terima_order" member-id = "{{$dataMember->id}}">Hapus</a> --}}

        <span class="custom-control-indicator"></span>
        @if($dataMember->terima_order==1)
          <span class="form-check-label text-success text-justify cek-teks-{{$dataMember->id}}">Terima Orderan</span>
        @else
          <span class="form-check-label text-danger text-justify cek-teks-{{$dataMember->id}}">Tidak Terima Orderan</span>
        @endif

    {{-- <input class="form-check-input" type="checkbox" role="switch" id="flexSwitchCheckChecked" checked> --}}
    {{-- <label class="form-check-label" for="flexSwitchCheckChecked">Terima Orderan </label> --}}
    </div>
    <div id="tampil">
        <p>Pastikan centang anda aktif menerima orderan, mohon matikan jika anda tidak siap menerima orderan.</p>
        <p>Masukkan Harga Jasa ( nama jasa ) Anda : </p>
        <div class="uk-margin" uk-margin>
            <div uk-form-custom="target: true">
                <input type="text" placeholder="Contoh : 50.000" >
            </div>
            <button class="uk-button uk-button-default">Submit</button>
        </div>
        <p>Harga Jasa Anda Sekarang : 50.000</p>
    </div>
</article>

<div class="container">
    <hr>
    <div class="uk-card uk-card-default uk-card-body ">
        <ul class="uk-nav-default uk-nav-parent-icon" uk-nav>
            <li class="uk-active"><a href="#">Tindakan</a></li>
            <li class="uk-parent">
                <a href="#">Penjelasan</a>
                <ul class="uk-nav-sub">
                    <li><a href="#" uk-toggle="target: #modal-pesanan" >Alur Mendapatkan Pesanan</a></li>
                    <li><a href="#" uk-toggle="target: #modal-penawaran">Alur Mengajukan Penawaran</a></li>
                    <li><a href="#" uk-toggle="target: #modal-iklan">Alur Mengajukan Periklanan</a></li>
                </ul>
            </li>
            <li class="uk-parent">
                <a href="#">Cara Aktivasi Akun</a>
                <ul class="uk-nav-sub">
                    <li><a href="#">1. Pastikan biodata anda sudah benar.</a></li>
                    <li><a href="#">2. Lakukan pembayaran biaya membership Rp. 50.000-, selama belum melakukan pembayaran maka status anda dibekukan.</a></li>
                    <li><a href="#">3. Upload bukti pembayaran pada menu lainnya.</a></li>
                </ul>
            </li>
            <li class="uk-nav-divider"></li>
            <li class="uk-nav-header">Lainnya</li>
            
            <li><a href="proyekPengajuan"><span class="uk-margin-small-right warnaBiru" uk-icon="icon: search"></span> Lihat Pengajuan Tersedia</a></li>
            <li><a href="{{route('editBiodataMember',Auth::user()->id)}}"><span class="uk-margin-small-right warnaBiru" uk-icon="icon: user"></span> Edit Biodata</a></li>
            <li><a href="buktiBayar"><span class="uk-margin-small-right warnaBiru" uk-icon="icon: cloud-upload"></span> Upload Bukti Pembayaran</a></li>
            <li><a href="https://wa.me/6289619715773"><span class="uk-margin-small-right warnaBiru" uk-icon="icon: receiver"></span>Hubungi CS</a></li>
        </ul>
    </div>
    </div>
    </div>
</div><br>

<div class="container">
<div class="d-grid gap-2">
<a href="{{route('IklankanMember',Auth::user()->id)}}" type="button" class="btn btn-outline-primary" uk-tooltip="Iklankan diri anda untuk dapat muncul di member teratas selama 30 hari.">Iklankan Diri Anda </a>
</div>
</div><br><br>

<!-- Tambah Gambar -->
<div class="container">
    <h2 class="text-center">Galeri Anda</h2>
    <p class="text-center" >Tambahkan galeri untuk memperkuat data anda bagi custmomer</p>
<div class="d-grid gap-2">
    <!-- Tombol Modal -->
    <button uk-toggle="target: #my-id" type="button" class="btn btn-outline-success" uk-tooltip="Tambahkan galeri untuk customer melihat anda.">Tambahkan galeri </button>
</div>
</div>
</div><br>

    <!-- Form Modal -->
    <div id="my-id" uk-modal>
        <div class="uk-modal-dialog uk-modal-body">
            <h2 class="uk-modal-title">Tambah Gambar</h2>
            <form action="{{ route('tambahGambarMember') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="uk-margin" uk-margin>
                <div uk-form-custom="target: true">
                    <input type="file" name="gambar" id="inputFile" multiple>
                    <input class="uk-input uk-form-width-medium" type="text" placeholder="Select file" disabled>
                </div>
                <button class="uk-button uk-button-default">Simpan</button>
                <button class="uk-button uk-button-default uk-modal-close" type="button">Batal</button>
            </div>
            </form>
        </div>
    </div>

<div class="container">
<div class="row row-cols-2 row-cols-md-4 g-4">

{{-- @php
    $galeri = DB::table('galeri')->where('id_user', Auth::user()->id)->get();
    $galeris = explode('|', $galeri->gambar)
@endphp

@foreach ($galeris as $item)
<div class="col">
    <div class="card h-100">
        <img src="{{ URL::to($item) }}" class="card-img-top" alt="...">
    </div>
</div>
@endforeach --}}
@foreach ($galeris as $item)
<div class="col">
    <div class="card h-100">
        <img src="../member/gambar/{{ $item->gambar }}" class="card-img-top" alt="...">
    </div>
</div>
@endforeach

</div>
</div>
</div>


<!-- Modal Alur Proyek Pilih Jasa/Tukang -->
<div id="modal-pesanan" uk-modal>
    <div class="uk-modal-dialog uk-modal-body">
        
        <p>Berikut adalah alur mendapatkan pesanan : </p>
        <ul>
        <li>Pastikan data yang anda masukkan benar.</li>
        <li>Pastikan centang anda aktif menerima orderan, mohon matikan jika anda tidak siap menerima orderan.</li>
        <li>Customer akan anda memilih bedasarkan data yang anda masukkan.</li>
        <li>Data yang terlihat pada customer dapat di cek pada lihat profile</li>
        <li>Jika anda dipilih kami akan menghubungi anda.</li>
        <li>Jika ACC maka proyek anda siap untuk dikerjakan.</li>
        </ul>
        
        <p class="uk-text-right">
            <button class="uk-button uk-button-default uk-modal-close" type="button">Tutup</button>
        </p>
        <br><br><br>
    </div>
</div>

<!-- Modal Alur Proyek Pilih Jasa/Tukang -->
<div id="modal-penawaran" uk-modal>
    <div class="uk-modal-dialog uk-modal-body">
        
        <p>Berikut adalah alur melakukan penawaran : </p>
        <ul>
        <li>Lihat penawaran pada Lihat Pengajuan Tersedia.</li>
        <li>Kirim berkas pengajuan anda dalam bentuk PDF lengkap dengan biodata perusahaan/pribadi anda beserta nomor yang dapat dihubungi.</li>
        <li>Setelah dikirim berkas tidak dapat dihapus, silakan kirim kembali jika terjadi kesalahan.</li>
        <li>Anda akan menunggu panggilan dari customer jika berkas anda dapat diterima. </li>
        <li>Anda dapat melihat riwayat penawaran pada tabel dibawah.</li>
        </ul>
        
        <p class="uk-text-right">
            <button class="uk-button uk-button-default uk-modal-close" type="button">Tutup</button>
        </p>
        <br><br><br>
    </div>
</div>

<!-- Modal Alur Periklanan -->
<div id="modal-iklan" uk-modal>
    <div class="uk-modal-dialog uk-modal-body">
        
        <p>Berikut adalah alur mengajukan periklanan: </p>
        <ul>
        <li>Tekan tombol iklankan diri anda.</li>
        <li>Lakukan pembayaran sebesar Rp. 50.000-, upload pada form iklankan diri anda.</li>
        <li>Kami akan mencatat dan melakukan iklan terhadap diri anda selama 30 hari penuh.</li>
        <li>Tombol tidak akan tersedia jika slot untuk iklan sudah penuh.</li>
        </ul>
        
        <p class="uk-text-right">
            <button class="uk-button uk-button-default uk-modal-close" type="button">Tutup</button>
        </p>
        <br><br><br>
    </div>
</div>
@endsection

@section('js')
<script>
$('.cek').change(function () {
    var val = $(this).val();
    var member_id = $(this).attr('member-id');
    console.log(member_id);
    if ($(this).is(':checked')) {
        $.get( "{{ url('/terimaOrder') }}/"+member_id+'/1')
          .done(function( data ) {
            // window.location.reload();
            $('.cek-teks-'+member_id).html('Terima Orderan');
            $('.cek-teks-'+member_id).addClass('text-success');
            $('.cek-teks-'+member_id).removeClass('text-danger');
            $('#tampil').hide();
            toastr.success('Menerima Orderan','Sukses',{timeout:1000});
          });
        // console.log(status);
        // console.log('diceklis');
    }else{
        $.get( "{{ url('/terimaOrder') }}/"+member_id+'/0')
          .done(function( data ) {
            // window.location.reload();
            $('.cek-teks-'+member_id).html('Tidak Terima Orderan');
            $('.cek-teks-'+member_id).removeClass('text-success');
            $('.cek-teks-'+member_id).addClass('text-danger');
            $('#tampil').show();
            $('.teks-'+member_id).html('');
            toastr.warning('Tidak Menerima Orderan','Sukses',{timeout:1000});
          });
        // console.log(status);            
        // console.log('ndak diceklis');            
    }
});
</script>
@endsection