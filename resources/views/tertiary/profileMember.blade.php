
@extends('layout/master')


@section('tittle', 'Profile')


@section('content')

<br><br>
<div class="container">

<article class="uk-comment uk-comment-primary">
    <header class="uk-comment-header">
        <div class="uk-grid-medium uk-flex-middle" uk-grid>
            <div class="uk-width-auto">
                <img class="uk-comment-avatar" src="{{ asset('asset/img/avatar.png') }}" width="80" height="80" alt="">
            </div>
            <div class="uk-width-expand">
                <h4 class="uk-comment-title uk-margin-remove"><a class="uk-link-reset" href="#">Nama Tukang</a></h4>
                <ul class="uk-comment-meta uk-subnav uk-subnav-divider uk-margin-remove-top">
                    <li><a href="#">Kategori Keahlian</a></li>
                    <li><a href="#">Kategori Wilayah</a></li>
                </ul>
            </div>
        </div>
    </header>
    <div class="uk-comment-body">
        <p>Biodata Member : </p>
        <ul>
        <li>Deskripsi : Lorem, ipsum dolor sit amet consectetur adipisicing elit. Ea velit quibusdam deleniti fugit ratione incidunt consectetur voluptatum at voluptas nesciunt. Nemo debitis eligendi dolor placeat fugit impedit id earum rerum?</li>
        <li>Pendidikan Terakhir : D3 Teknik Sipil</li>
        <li>Harga Jasa : 50.000</li>
        </ul>
    </div>
</article>

<div class="container">
    <hr>
    <button class="uk-button uk-button-default" type="button">Tambahkan Ke Proyek</button>
    <div uk-dropdown>
        <ul class="uk-nav uk-dropdown-nav">
            <li class="uk-active"><a href="#">Pilih Proyek</a></li>
            <li><a href="#">Pengaspalan Jalan</a></li>
            <!-- Misalkan customer punya proyek lain aktif -->
            <li><a href="#">Pemasangan Listrik</a></li>
            <!-- Selesai proyek lain aktif -->
        </ul>
    </div><p></p>
    <a class="btn btn-outline-primary" href="#" role="button">Lihat Sertifikat Keahlian</a>
    <a class="btn btn-outline-warning" href="https://wa.me/6289619715773" role="button">Hubungi CS</a>
<br>
<h1 class="uk-heading-line uk-text-center fw-bold"><span>Galeri Member</span></h1>
    <hr>

    <div class="uk-position-relative uk-visible-toggle uk-dark" tabindex="-1" uk-slider="center: true" uk-slider="autoplay: true">

    <div class="container">
    <div class="row row-cols-2 row-cols-md-4 g-4">
    <div class="col">
        <div class="card h-100">
        <img src="{{ asset('asset/img/avatar.png') }}" class="card-img-top" alt="...">
        </div>
    </div>
    <div class="col">
        <div class="card h-100">
        <img src="{{ asset('asset/img/avatar.png') }}" class="card-img-top" alt="...">
        </div>
    </div>
    <div class="col">
        <div class="card h-100">
        <img src="{{ asset('asset/img/avatar.png') }}" class="card-img-top" alt="...">
        </div>
    </div>
    <div class="col">
        <div class="card h-100">
        <img src="{{ asset('asset/img/avatar.png') }}" class="card-img-top" alt="...">
    </div>
    </div>
    </div>
</div>

</div>

<br><br>
<div class="container">
<h1 class="uk-heading-line uk-text-center fw-bold"><span>Lihat Lainnya</span></h1>

<div class="row row-cols-1 row-cols-md-3 g-4">
  <div class="col">
    <div class="card h-100">
    <div class="uk-card-header">
        <div class="uk-grid-small uk-flex-middle" uk-grid>
            <div class="uk-width-auto">
                <img class="uk-border-circle" width="40" height="40" src="{{ asset('asset/img/avatar.png') }}">
            </div>
            <div class="uk-width-expand">
                <h3 class="uk-card-title uk-margin-remove-bottom">Nama Tukang</h3>
                <p class="uk-text-meta uk-margin-remove-top">Tukang Listrik | Wilayah</p>
            </div>
        </div>
    </div>
    <div class="uk-card-body">
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.</p>
    </div>
    <div class="uk-card-footer">
        <a href="profileMember" class="uk-button uk-button-text">Pesan Jasa</a>
    </div>
    </div>
  </div>

<!-- Hapus dari sini -->
  <div class="col">
    <div class="card h-100">
    <div class="uk-card-header">
        <div class="uk-grid-small uk-flex-middle" uk-grid>
            <div class="uk-width-auto">
                <img class="uk-border-circle" width="40" height="40" src="{{ asset('asset/img/avatar.png') }}">
            </div>
            <div class="uk-width-expand">
                <h3 class="uk-card-title uk-margin-remove-bottom">Nama Tukang</h3>
                <p class="uk-text-meta uk-margin-remove-top">Kategori | Kuburaya</p>
            </div>
        </div>
    </div>
    <div class="uk-card-body">
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.</p>
    </div>
    <div class="uk-card-footer">
        <a href="#" class="uk-button uk-button-text">Pesan Jasa</a>
    </div>
    </div>
  </div>

  <div class="col">
    <div class="card h-100">
    <div class="uk-card-header">
        <div class="uk-grid-small uk-flex-middle" uk-grid>
            <div class="uk-width-auto">
                <img class="uk-border-circle" width="40" height="40" src="{{ asset('asset/img/avatar.png') }}">
            </div>
            <div class="uk-width-expand">
                <h3 class="uk-card-title uk-margin-remove-bottom">Nama Tukang</h3>
                <p class="uk-text-meta uk-margin-remove-top">Kategori | Wilayah</p>
            </div>
        </div>
    </div>
    <div class="uk-card-body">
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.</p>
    </div>
    <div class="uk-card-footer">
        <a href="#" class="uk-button uk-button-text">Pesan Jasa</a>
    </div>
    </div>
  </div>

  <div class="col">
    <div class="card h-100">
    <div class="uk-card-header">
        <div class="uk-grid-small uk-flex-middle" uk-grid>
            <div class="uk-width-auto">
                <img class="uk-border-circle" width="40" height="40" src="{{ asset('asset/img/avatar.png') }}">
            </div>
            <div class="uk-width-expand">
                <h3 class="uk-card-title uk-margin-remove-bottom">Nama Tukang</h3>
                <p class="uk-text-meta uk-margin-remove-top">Kategori | Wilayah</p>
            </div>
        </div>
    </div>
    <div class="uk-card-body">
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.</p>
    </div>
    <div class="uk-card-footer">
        <a href="#" class="uk-button uk-button-text">Pesan Jasa</a>
    </div>
    </div>
  </div>
  <!-- sampai sini -->

</div>

</div></div>
</div><br>

@endsection