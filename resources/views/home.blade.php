
@extends('layout/masterPortal')


@section('tittle', 'Portal')



@section('content')

<img src="{{ asset('asset/img/jakilat.png') }}" class="gambarlogo2 rounded mx-auto d-block gambarlogo" alt="...">



<div class="container">

<p class="text-center"> Selamat datang di portal JAKILAT, silahkan pilih menu yang anda inginkan</p>

<div class="uk-child-width-1-2@s uk-grid-match text-center" uk-grid>
    <div>
        <a href="berandaUtama">
        <div class="uk-card uk-card-hover uk-card-body shadow">
            <h4 class="uk-card-title">
            <p uk-icon="cart" ></p>
            Proyek</h4>
            <p>Ayo Cari Atau Ajukan Jasa Yang Anda Butuhkan</p>
        </div>
        </a>
    </div>
    <div>
    <a href="tentangKami">
        <div class="uk-card uk-card-hover uk-card-body shadow">
            <h4 class="uk-card-title">
            <p uk-icon="info" ></p>
            TENTANG KAMI
            </h4>
            <p>Yuk Cek Tentang Kami</p>
        </div>
        </a>
    </div>
    <div>
    <a href="gabungMember">
        <div class="uk-card uk-card-hover uk-card-body shadow">
            <h4 class="uk-card-title">
            <p uk-icon="users" ></p>
            GABUNG MEMBER
            </h4>
            <p>Gabung Member Raih Pemasukan Lebih Dari Usaha Kamu</p>
        </div>
        </a>
    </div>
    <div>
    <a href="ketentuan">
        <div class="uk-card uk-card-hover uk-card-body shadow">
            <h4 class="uk-card-title">
            <p uk-icon="bell" ></p>
            PRIVACY POLICY
            </h4>
            <p>Kami Selalu Menjaga Anda</p>
        </div>
        </a>
    </div>
</div>
</div>

@endsection