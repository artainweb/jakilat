<?php

namespace App\Http\Controllers;

use App\Models\User;

class primaryController extends Controller
{

    public function index()
    {
        return view('home');
    }

    public function tentangKami()
    {
        return view('tentangKami');
    }

    public function gabungMember()
    {
        $user = User::get();
        return view('gabungMember',compact('user'));
    }

    public function gabungCustomer()
    {
        return view('gabungCustomer');
    }

    public function ketentuan()
    {
        return view('ketentuan');
    }

    public function login()
    {
        return view('login');
    }

    public function berandaUtama()
    {
        return view('berandaUtama');
    }
}
