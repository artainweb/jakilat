<?php

namespace App\Http\Controllers;

use App\Models\Galeri;
use App\Models\DetailUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class tertiaryController extends Controller
{
    public function berandaMember()
    {
        $galeris = Galeri::where('id_user', Auth()->user()->id)->get();
        $dataMember = DetailUser::where('id_user', Auth()->user()->id)->first();
        return view('tertiary.berandaMember', compact('dataMember', 'galeris'));
    }

    public function profileMember()
    {
        return view('tertiary.profileMember');
    }

    public function editBiodataMember()
    {
        return view('tertiary.editBiodataMember');
    }

    public function iklan()
    {
        return view('tertiary.iklan');
    }

    public function proyekPengajuan()
    {
        return view('tertiary.proyekPengajuan');
    }
}
