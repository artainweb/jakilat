<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class secondaryController extends Controller
{
    public function beranda()
    {
        return view('secondary.beranda');
    }

    public function deskripsi()
    {
        return view('secondary.deskripsi');
    }

    public function editBiodata()
    {
        return view('secondary.editBiodata');
    }

    public function editProyek()
    {
        return view('secondary.editProyek');
    }

    public function tambahProyek()
    {
        return view('secondary.tambahProyek');
    }

    public function buktiBayar()
    {
        return view('secondary.buktiBayar');
    }

    public function detailProyekPengajuan()
    {
        return view('secondary.detailProyekPengajuan');
    }

    public function tambahProyekPengajuan()
    {
        return view('secondary.tambahProyekPengajuan');
    }
}
