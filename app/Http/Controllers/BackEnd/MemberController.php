<?php

namespace App\Http\Controllers\BackEnd;

use App\Models\User;
use App\Models\Galeri;
use App\Models\DetailUser;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Requests\EditRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;

class MemberController extends Controller
{
    public function terima_order($id, $terima_order){

    $terima_orders=  DetailUser::find($id); //Mengambil data siswa dengan ID yang ada di parameter ....., $id
    $terima_orders->update([
        'terima_order'=>$terima_order
    ]);
    return redirect()->back()->with('success');
    }

    public function edit_biodata_member($id){
        $dataMember = DetailUser::where('id_user',$id)->first();
        return view('tertiary.editBiodataMember',compact('dataMember'));
    }

    public function update_biodata_member(EditRequest $request, $id)
    {
        $dataMember = DetailUser::where('id_user',$id)->first();
        $dataMember->pendidikan_terakhir = $request->pendidikan_terakhir;
        $dataMember->keahlian = $request->keahlian;
        $dataMember->alamat = $request->alamat;
        $dataMember->wilayah = $request->wilayah;
        $dataMember->deskripsi_diri = $request->deskripsi_diri;
        $dataMember->nomor_hp = $request->nomor_hp;
        
        $dataMember = User::find($id);
        $dataMember->email = $request->email;
        $dataMember->name = $request->name;

        $kodeRandom = Str::random(6);
        
        if ($request->hasFile('ktp')) {
            $path_ktp = 'member/ktp' . $dataMember->ktp;
            if (File::exists($path_ktp)) {
                File::delete($path_ktp);
            }
            $file = $request->file('ktp');
            $ekstensi = $file->getClientOriginalExtension();
            $filename = time() . $kodeRandom .'.' . $ekstensi;
            $file->move('member/ktp', $filename);
			// Image::make($file->getRealPath())->resize(500, 500)->save($path);
			$dataMember->ktp = $filename;
        }

        if ($request->hasFile('berkas_persyaratan')) {
            $path_berkas_persyaratan = 'member/berkas_persyaratan' . $dataMember->berkas_persyaratan;
            if (File::exists($path_berkas_persyaratan)) {
                File::delete($path_berkas_persyaratan);
            }
            $file = $request->file('berkas_persyaratan');
            $ekstensi = $file->getClientOriginalExtension();
            $filename = time() . $kodeRandom .'.' . $ekstensi;
            $file->move('member/berkas_persyaratan', $filename);
			// Image::make($file->getRealPath())->resize(500, 500)->save($path);
			$dataMember->berkas_persyaratan = $filename;
        }

        if ($request->hasFile('sertifikat_keterampilan')) {
            $path_sertifikat_keterampilan = 'member/sertifikat_keterampilan' . $dataMember->sertifikat_keterampilan;
            if (File::exists($path_sertifikat_keterampilan)) {
                File::delete($path_sertifikat_keterampilan);
            }
            $file = $request->file('sertifikat_keterampilan');
            $ekstensi = $file->getClientOriginalExtension();
            $filename = time() . $kodeRandom .'.' . $ekstensi;
            $file->move('member/sertifikat_keterampilan', $filename);
			// Image::make($file->getRealPath())->resize(500, 500)->save($path);
			$dataMember->sertifikat_keterampilan = $filename;
        }

        if ($request->hasFile('rekomendasi')) {
            $path_rekomendasi = 'member/rekomendasi' . $dataMember->rekomendasi;
            if (File::exists($path_rekomendasi)) {
                File::delete($path_rekomendasi);
            }
            $file = $request->file('rekomendasi');
            $ekstensi = $file->getClientOriginalExtension();
            $filename = time() . $kodeRandom .'.' . $ekstensi;
            $file->move('member/rekomendasi', $filename);
			// Image::make($file->getRealPath())->resize(500, 500)->save($path);
			$dataMember->rekomendasi = $filename;
        }

        if ($request->hasFile('foto_diri')) {
            $path_foto_diri = 'member/foto_diri' . $dataMember->foto_diri;
            if (File::exists($path_foto_diri)) {
                File::delete($path_foto_diri);
            }
            $file = $request->file('foto_diri');
            $ekstensi = $file->getClientOriginalExtension();
            $filename = time() . $kodeRandom .'.' . $ekstensi;
            $file->move('member/foto_diri', $filename);
			// Image::make($file->getRealPath())->resize(500, 500)->save($path);
			$dataMember->foto_diri = $filename;
        }
        $dataMember->save();
        toast('Berhasil Merubah Biodata','success');
        return redirect('berandaMember')->with('success');
    }

    public function tambah_gambar(Request $request){

            $galeri = new Galeri;
            $galeri->id_user = Auth::user()->id;
            if ($request->hasFile('gambar')) {
                $file = $request->file('gambar');
                $ekstensi = $file->getClientOriginalExtension();
                $filename = time() . 'ID'.Auth::user()->id .'.' . $ekstensi;
                $file->move('member/gambar', $filename);
                $galeri->gambar = $filename;
            }
            $galeri->save();

            // $gambar = array();
            // // $galeri->keterangan = $request->keterangan;
            // if ($files = $request->file('gambar')) {
            //    foreach ($files as $file){
            //        $nama_gambar = md5(rand(1000, 100000));
            //        $ekstensi = strtolower($file->getClientOriginalExtension());
            //        $nama_lengkap_gambar = $nama_gambar.'.'.$ekstensi;
            //        $upload = 'member/gambar/';
            //        $gambar_url = $upload.$nama_lengkap_gambar;
            //        $file->move($upload, $nama_lengkap_gambar);
            //        $gambar[] = $gambar_url;
            //    }
            // }
            // Galeri::insert([
            //     'id_user' => Auth::user()->id,
            //     'gambar' =>implode('|', $gambar)
            // ]);

        toast('Berhasil Menambah Gambar','success');
        return redirect('berandaMember')->with('success');
    }
}
