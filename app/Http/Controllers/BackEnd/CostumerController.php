<?php

namespace App\Http\Controllers\BackEnd;

use App\Models\User;
use App\Models\DetailUser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class CostumerController extends Controller
{
        public function edit_biodata_costumer($id){
            $dataCostumer = DetailUser::where('id_user',$id)->first();
            return view('tertiary.editBiodata',compact('dataCostumer'));
        }
    
        public function update_biodata_costumer(InputRequest $request, $id)
        {
            $dataCostumer = User::find($id);
            $dataCostumer->email = $request->email;
            $dataCostumer->name = $request->name;
            $dataCostumer->password = Hash::make($request->password);

            $dataCostumer = DetailUser::find($id);
            $dataCostumer->alamat = $request->alamat;
            $dataCostumer->nomor_hp = $request->nomor_hp;
            $dataCostumer->save();
            toast('Anda berhasil mendaftar sebagai Customer, Silahkan tunggu Konfirmasi dari Admin','success');
            return redirect()->back()->with('success');
        }
}
