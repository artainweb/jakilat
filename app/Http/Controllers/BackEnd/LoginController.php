<?php

namespace App\Http\Controllers\BackEnd;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function postlogin(Request $request){
        $request->validate([

            'email' => 'required|email',
            'password' => 'required',
        ]);

        if(Auth::attempt($request->only('email','password','role'))){
            if (auth()->user()->role == 'Customer') {
                return redirect()->route('beranda');
            } 
            elseif (auth()->user()->role == 'Member') {
                return redirect()->route('berandaMember');
            }
        }else{
            toast('Gagal Login, <br> <small>Cek kembali Email, Password dan Jenis akun anda</small>','error');
            return redirect()->route('login')
                ->with('error','Email-Address And Password Are Wrong.');
        }
    }
}
